# 这是一个简单的管理项目

## 技术栈: 
```
Vue+Element(html+css+js)+nodejs+MongoDB+mongose+axios+es6

```
## 数据工具:


```  
mongobooster+Postman
```


## axios的安装配置(已经全局安装),$是为了防止重复,调用的时候也都用axios

```
import axios from 'axios'
Vue.prototype.$http = axios.create({
  baseRUL:'http://localhost:3000/api'
})
```