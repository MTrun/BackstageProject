const express = require('express')
const app = express()

// 设置跨域模型,一个端口给前端一个给后端,记得多安装一个cnpm i cors
app.use(require('cors')())
// 提示客户端识别json数据
app.use(express.json())

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/element-admin', {
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true,
})

const Article = mongoose.model('Article', new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
}))

app.get('/', async (request, response) => {
    response.send('index')
})

//新增文章:'/api/资源+s',create创建数据save也可以
app.post('/api/articles', async (request, response) => {
    const article = await Article.create(request.body)
    response.send(article)
})

//文章列表
app.get('/api/articles', async (request, response) => {
    const articles = await Article.find()
    response.send(articles)
})

//删除文章
// app.delete('/api/articles/:id', async(request,response)=>{
//     await Article.findByIdAndDelete(response.params.id)
//     response.send({
//         status: true
//     })
// })

//删除文章
app.delete('/api/articles/:id', function (request, response) {
    Article.findByIdAndDelete(request.params.id, function () {
        response.send({
            status: true
        })
    })

})

//获取编辑的详情页
app.get('/api/articles/:id', async (request, response) => {
    const articles = await Article.findById(request.params.id)
    response.send({
        articles
    })
})

//修改文章
app.put('/api/articles/:id', async (request, response) => {
    const articles = await Article.findByIdAndUpdate(request.params.id, request.body)
    response.send(articles)
})

app.listen(3000, () => {
    console.log("http://localhost:3000/");
})