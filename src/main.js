import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import Router from 'vue-router'

// 防止多次点击路由报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(ElementUI);
Vue.config.productionTip = false

//使用axios直接调用$http
import axios from 'axios'
Vue.prototype.$http = axios.create({
  baseURL:'http://localhost:3000/api'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
