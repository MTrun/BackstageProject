import Vue from 'vue'
import Router from 'vue-router'
import CreateArticle from './views/CreateArticle.vue'
import EditeArticle from './views/EditArticle.vue'
import ListArticle from './views/ListArticle.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/articles/index'
    },
    {
      path: '/articles/index',
      name: 'list-article',
      component: ListArticle
    },
    {
      path: '/articles/:id/edit',
      name: 'edit-article',
      component: EditeArticle
    },
    {
      path: '/posts/create',
      name: 'create-article',
      component: CreateArticle
    }
  ]
})
